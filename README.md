# Simple eCommerce Offers Proof of Concept by Matthew McGahan

This is a project to demonstrate basic offer functionality for a simulated eCommerce platform featuring offers.

## Installation

Clone repo from bitbucket at this URL:
git clone https://bitbucket.org/mmcgahan1/simple-ecommerce-api.git

After successfully cloning, start the Spring Application by running maven in the main simple-ecommerce-rest-api folder by using the command (RECCOMENDED):

```bash
mvn spring-boot:run
```
Alternatively run the java .jar file from the same main folder by using this command from CMD:

```shell
java -jar target/simple-ecommerce-rest-api-0.0.1-SNAPSHOT
```

## Usage

## Endpoints

There are 2 endpoints GET /offer and POST /offer. By Default starting the application with the mvn spring-boot:run command means that you can shoot JSON requests to localhost:8080.

### POST /offer

Offers must be created in the POST /offer endpoint. A valid offer must contain the following parameters: ID, List Price, Currency Type, Description and an End Time.

These are set as JSON in the body of the POST request. The parameters that must be sent and their formats are as follows:

"listPrice"
	Sent as a string containing a BigDecimal value between 1 and 99999 inclusive
"currency"
	Sent as a string containing a 3 letter currency code e.g. GBP
"itemDescription"
	Sent as a string, between 20 and 400 characters in length.

For the End Time either one of two parameters must be sent:
	"duration"
		Sent as a string, duration in minutes for the offer to be valid for from present moment can be between 60 and 43200 inclusive
	"endTime"
		Sent as a string, dates in following format yyyy-MM-ddTHH:mm:ss, valid date range is between beginning of 2010 to the end of 2029

Example Input
```JSON
{
	
	"listPrice": "111",

	"currency": "GBP",

	"itemDescription": "A very nice Bone China vase with aquamarine paint.",

	"endTime": "2022-12-12T23:59:59"

}
```
The ID is set internally and is returned in the response.

Example Output
```JSON
{
	
	"id": 1,

	"listPrice": "111",

	"currency": "GBP",

	"itemDescription": "A very nice Bone China vase with aquamarine paint.",

	"endTime": "2022-12-12T23:59:59"

}
```

### GET /offer

The GET /offer Endpoint is simple to use. To retrieve a previously created offer put the id query param in the url of the id you wish to retrieve.
for example a valid url would look like this with the default setup: localhost:8080/offer?id=1

## Testing

In the Postman API Tests folder there are a range of postman tests that cover both endpoints for a variety of different error cases and success cases. Feel Free to use these to easily test the code when it is running.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)