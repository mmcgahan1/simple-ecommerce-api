package com.mmcgahan.simpleecommercerestapi.offerendpoint.utils;

import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorCodes;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorConstants;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.OfferError;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.PostOfferRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class OfferUtilsTest {

    private static final String CORRECT_ITEM_DESCRIPTION = "A Reasonable length item Description";
    private static final String INCORRECT_ITEM_DESCRIPTION = "Short Description";

    private static final String CORRECT_CURRENCY = "GBP";
    private static final String INCORRECT_CURRENCY = "G5X";

    private static final String CORRECT_DURATION = "60";
    private static final String INCORRECT_DURATION = "555555";

    private static final String CORRECT_ENDTIME = "2025-12-30T12:34:56";
    private static final String INCORRECT_ENDTIME = "2031-12-30T12:34:56";

    private static final String CORRECT_LISTPRICE = "100";
    private static final String INCORRECT_LISTPRICE = "100000";
    private static final String DECIMAL_LISTPRICE = "100.00";

    @Test
    public void testErrorMissingItemDescription() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                null,
                null,
                CORRECT_ENDTIME);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.MISSING_PARAMETERS_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.MISSING_PARAMETERS, offerError.getTitle());
        Assert.assertEquals("The mandatory parameter(s) [itemDescription] are not provided",
                offerError.getErrorDescription());
    }

    @Test
    public void testErrorMissingEndTimeAndDuration() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                null,
                CORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.MISSING_PARAMETERS_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.MISSING_PARAMETERS, offerError.getTitle());
        Assert.assertEquals("The mandatory parameter(s) [endTime or duration] are not provided",
                offerError.getErrorDescription());
    }

    @Test
    public void testSuccessOnlyDurationWithNoEndTime() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                CORRECT_DURATION,
                CORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertNull(offerError);
    }

    @Test
    public void testErrorIncorrectDuration() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                INCORRECT_DURATION,
                CORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.INCORRECT_PARAMETER_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.INCORRECT_PARAMETER, offerError.getTitle());
        Assert.assertEquals("Duration is in minutes and must be greater than 60 and less than 43200",
                offerError.getErrorDescription());
    }

    @Test
    public void testErrorIncorrectEndTime() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                null,
                CORRECT_ITEM_DESCRIPTION,
                INCORRECT_ENDTIME);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.INCORRECT_PARAMETER_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.INCORRECT_PARAMETER, offerError.getTitle());
        Assert.assertEquals("End time must match the following REGEX expression :"
                        + " .*[2][0][1-2][0-9][-][0-1][0-9][-][0-3][0-9][T][0-2][0-4][:][0-6][0-9][:][0-6][0-9].*",
                offerError.getErrorDescription());
    }
    @Test
    public void testErrorIncorrectCurrency() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                INCORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                null,
                CORRECT_ITEM_DESCRIPTION,
                CORRECT_ENDTIME);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.INCORRECT_PARAMETER_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.INCORRECT_PARAMETER, offerError.getTitle());
        Assert.assertEquals("Currency must match the following REGEX expression :"
                        + " .*[a-zA-Z]{3}.*",
                offerError.getErrorDescription());
    }

    @Test
    public void testErrorIncorrectListPrice() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                INCORRECT_LISTPRICE,
                CORRECT_DURATION,
                CORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.INCORRECT_PARAMETER_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.INCORRECT_PARAMETER, offerError.getTitle());
        Assert.assertEquals("List Price must have a value between 1 and 99999",
                offerError.getErrorDescription());
    }

    @Test
    public void testErrorDecimalListPrice() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                DECIMAL_LISTPRICE,
                CORRECT_DURATION,
                CORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertNull(offerError);
    }

    @Test
    public void testErrorIncorrectItemDescription() {
        PostOfferRequest postOfferRequest = buildPostOfficeRequest(
                CORRECT_CURRENCY,
                CORRECT_LISTPRICE,
                CORRECT_DURATION,
                INCORRECT_ITEM_DESCRIPTION,
                null);
        OfferError offerError = OfferUtils.isValid(postOfferRequest);
        Assert.assertEquals(OfferErrorCodes.INCORRECT_PARAMETER_CODE, offerError.getCode());
        Assert.assertEquals(OfferErrorConstants.INCORRECT_PARAMETER, offerError.getTitle());
        Assert.assertEquals("Item Description must be between 20 and 400 characters long",
                offerError.getErrorDescription());
    }

    private PostOfferRequest buildPostOfficeRequest(String currency, String listPrice, String duration,
                                                    String itemDescription, String endTime) {
        PostOfferRequest postOfferRequest = new PostOfferRequest();
        postOfferRequest.setCurrency(currency);
        postOfferRequest.setId(1);
        postOfferRequest.setItemDescription(itemDescription);
        postOfferRequest.setListPrice(listPrice);
        if (duration != null) {
            postOfferRequest.setDuration(Integer.parseInt(duration));
        }
        if (endTime != null) {
            postOfferRequest.setEndTime(endTime);
        }
        return postOfferRequest;
    }

}