package com.mmcgahan.simpleecommercerestapi.offerendpoint.models;

public class OfferError {

    int code;

    String title;

    String errorDescription;

    public OfferError(int code, String title, String errorDescription) {
        this.code = code;
        this.title = title;
        this.errorDescription = errorDescription;
    }
    public OfferError(int code, String title) {
        this.code = code;
        this.title = title;
        this.errorDescription = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
