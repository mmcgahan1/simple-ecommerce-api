package com.mmcgahan.simpleecommercerestapi.offerendpoint.constants;

public class OfferErrorCodes {

    public static final int MISSING_PARAMETERS_CODE = 100;

    public static final int INCORRECT_PARAMETER_CODE = 101;

    public static final int DUPLICATE_OFFER_CODE = 102;

    public static final int NO_OFFER_FOUND_CODE = 103;

    public static final int OFFER_EXPIRED_CODE = 104;

}
