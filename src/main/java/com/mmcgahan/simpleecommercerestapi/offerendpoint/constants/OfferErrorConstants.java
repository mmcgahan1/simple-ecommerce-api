package com.mmcgahan.simpleecommercerestapi.offerendpoint.constants;

public class OfferErrorConstants {

    public final static String MISSING_PARAMETERS = "Missing mandatory parameters";

    public final static String INCORRECT_PARAMETER = "Parameter provided in incorrect format";

    public final static String DUPLICATE_OFFER = "Offer is duplicate to existing offer";

    public final static String NO_OFFER_FOUND = "No offer found";

    public final static String OFFER_EXPIRED = "Offer has expired";

}
