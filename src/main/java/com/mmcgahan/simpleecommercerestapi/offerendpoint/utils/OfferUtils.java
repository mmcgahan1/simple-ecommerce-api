package com.mmcgahan.simpleecommercerestapi.offerendpoint.utils;

import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.OfferError;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.PostOfferRequest;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorCodes;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorConstants;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.PostOfferResponse;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class OfferUtils {

    private static final String CURRENCY_REGEX = ".*[a-zA-Z]{3}.*";

    private static final String ENDTIME_REGEX = ".*[2][0][1-2][0-9][-][0-1][0-9][-][0-3][0-9][T][0-2][0-4][:][0-6][0-9][:][0-6][0-9].*";

    private static final String ENDTIME_INVALID_REGEX_ERROR_MESSAGE = "End time must match the following REGEX expression : "
            + ENDTIME_REGEX.toString();

    private static final String CURRENCY_INVALID_ERROR_MESSAGE = "Currency must match the following REGEX expression : "
            + CURRENCY_REGEX.toString();

    private static final String DURATION_INVALID_ERROR_MESSAGE = "Duration is in minutes and must be greater than 60 and less than 43200";

    private static final String ENDTIME_INVALID_ERROR_MESSAGE = "End time must be at least 60 minutes in the future";

    private static final String LIST_PRICE_INVALID_ERROR_MESSAGE = "List Price must have a value between 1 and 99999";

    private static final String ITEM_DESCRIPTION_INVALID_ERROR_MESSAGE = "Item Description must be between 20 and 400 characters long";

    public static OfferError isValid(PostOfferRequest postOfferRequest) {
        OfferError offerError = containsAllMandatoryParameters(postOfferRequest);

        if (offerError == null) {
            offerError = areAllParametersFormattedCorrectly(postOfferRequest);
        }

        if (offerError != null) {
            return offerError;
        }
        return null;
    }

    private static OfferError containsAllMandatoryParameters(PostOfferRequest postOfferRequest) {
        List<String> missingParams = new ArrayList<>();
        if (postOfferRequest.getCurrency() == null) {
            missingParams.add("currency");
        }
        if (postOfferRequest.getItemDescription() == null) {
            missingParams.add("itemDescription");
        }
        if (postOfferRequest.getListPrice() == null) {
            missingParams.add("listPrice");
        }
        if (postOfferRequest.getEndTime() == null) {
            if (postOfferRequest.getDuration() == null) {
                missingParams.add("endTime or duration");
            }
        }
        if (!CollectionUtils.isEmpty(missingParams)) {
            String errorMessage = "The mandatory parameter(s) " + missingParams.toString() + " are not provided";
            return new OfferError(
                    OfferErrorCodes.MISSING_PARAMETERS_CODE,
                    OfferErrorConstants.MISSING_PARAMETERS,
                    errorMessage);
        }
        return null;
    }
    private static OfferError areAllParametersFormattedCorrectly(PostOfferRequest postOfferRequest) {
        if (new BigDecimal(postOfferRequest.getListPrice()).compareTo(new BigDecimal(1)) < 0
                || new BigDecimal(postOfferRequest.getListPrice()).compareTo(new BigDecimal(99999)) > 0) {
            return createInvalidOfferError(LIST_PRICE_INVALID_ERROR_MESSAGE);
        }
        if (postOfferRequest.getItemDescription().length() > 400
                || postOfferRequest.getItemDescription().length() < 20) {
            return createInvalidOfferError(ITEM_DESCRIPTION_INVALID_ERROR_MESSAGE);
        }
        if (postOfferRequest.getDuration() != null) {
            if (postOfferRequest.getDuration() < 60 || postOfferRequest.getDuration() > 43200) {
                return createInvalidOfferError(DURATION_INVALID_ERROR_MESSAGE);
            }
        }
        if (postOfferRequest.getEndTime() != null) {
            //Assuming server always runs on GMT+0
            if (!Pattern.matches(ENDTIME_REGEX, postOfferRequest.getEndTime())) {
                return createInvalidOfferError(ENDTIME_INVALID_REGEX_ERROR_MESSAGE);
            }
            if (LocalDateTime.parse(postOfferRequest.getEndTime()).minusMinutes(59L).compareTo(LocalDateTime.now()) < 0) {
                return createInvalidOfferError(ENDTIME_INVALID_ERROR_MESSAGE);
            }
        }
        if (postOfferRequest.getCurrency().length() == 3 && !Pattern.matches(CURRENCY_REGEX, postOfferRequest.getCurrency())) {
            return createInvalidOfferError(CURRENCY_INVALID_ERROR_MESSAGE);
        }
        return null;
    }

    private static OfferError createInvalidOfferError(String invalidParameterMessage) {
        return new OfferError(
                OfferErrorCodes.INCORRECT_PARAMETER_CODE,
                OfferErrorConstants.INCORRECT_PARAMETER,
                invalidParameterMessage);
    }

    public static PostOfferResponse mapPostOfferResponse(PostOfferRequest inputPayload, Integer id) {
        PostOfferResponse response = new PostOfferResponse();
        response.setId(id);
        response.setItemDescription(inputPayload.getItemDescription());
        response.setCurrency(inputPayload.getCurrency());
        response.setListPrice(inputPayload.getListPrice());
        LocalDateTime endTime = calculateEndTime(inputPayload.getEndTime(), inputPayload.getDuration());
        if (endTime != null) {
            response.setEndTime(endTime);
        }
        return response;
    }
    public static LocalDateTime calculateEndTime(String endTime, Integer duration) {
        if (endTime != null) {
            return  LocalDateTime.parse(endTime);
        }
        if (duration != null) {
            return LocalDateTime.now().plusMinutes(duration);
        }
        return null;
    }
}
