package com.mmcgahan.simpleecommercerestapi.offerendpoint.controller;

import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorCodes;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.constants.OfferErrorConstants;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.OfferError;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.PostOfferRequest;
import com.mmcgahan.simpleecommercerestapi.offerendpoint.models.PostOfferResponse;

import com.mmcgahan.simpleecommercerestapi.offerendpoint.utils.OfferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class WebController {

    Map<Integer, PostOfferResponse> listOfOffers;

    AtomicInteger currentId;


    @RequestMapping(value = "/offer", method = RequestMethod.POST)
    public ResponseEntity createOffer(@RequestBody PostOfferRequest inputPayload) {

        //Limitation of current program is only Integer.MAX_VALUE-1 Offers can be made
        // before offers are placed over old ones
        if (currentId == null || currentId.intValue() == Integer.MAX_VALUE) {
            this.currentId = new AtomicInteger(1);
        }

        OfferError offerError = OfferUtils.isValid(inputPayload);
        if (offerError != null) {
            return ResponseEntity.badRequest().body(offerError);
        }
        //If Request is fully valid, map response
        PostOfferResponse response = OfferUtils.mapPostOfferResponse(inputPayload, currentId.getAndIncrement());

        if (listOfOffers == null) {
            listOfOffers = new HashMap<>();
            listOfOffers.put(response.getId(), response);
        } else {
            listOfOffers.put(response.getId(), response);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @RequestMapping(value = "/offer", method = RequestMethod.GET)
    public ResponseEntity retrieveOffer(@RequestParam(value = "id") String id) {
        PostOfferResponse response;

        if (listOfOffers.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        if (listOfOffers.get(Integer.parseInt(id)) != null) {
            PostOfferResponse offer = listOfOffers.get(Integer.parseInt(id));
            if (offer.getEndTime().compareTo(LocalDateTime.now()) > 0) {
                response = offer;
            } else {
                OfferError offerError = new OfferError(
                        OfferErrorCodes.OFFER_EXPIRED_CODE,
                        OfferErrorConstants.OFFER_EXPIRED,
                        "Offer id: " + id + " has expired");
                return ResponseEntity.badRequest().body(offerError);
            }
        } else {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(response);
    }
}
