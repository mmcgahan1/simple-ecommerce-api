package com.mmcgahan.simpleecommercerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleEcommerceRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleEcommerceRestApiApplication.class, args);
	}

}
